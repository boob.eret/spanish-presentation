# Tiempo Viejo

## En España
- En el 2ª república había mucho cambio - leyes para divorcio y uniones civiles
- Reforme fundamental de la sociedad a una sociedad que no hubiera sido tan distinta a la de hoy

- Con Franco la influencia de la iglesia católica era total, y por eso el divorcio se prohibió. Los derechos fundamentales de mujeres no existieron, y era como eran la propiedad de su padre o marido. 
 - Después de la transición se aprobó un ley que permitió el divorcio, pero era una cosa tan difícil y tan complicado. 
 - Solo era posible tener un divorcio unilateral después de 5 años de vivir sin cohabitación. 


## En Reino Unido
-  Se necesitó un "proyecto de ley de interés local", que necesitó de verdad un acto de Parlamento
- Permitió que la gente pudo tener un divorcio con un juez civil, no en los cortes eclesiásticos
- Dio a las mujeres la capacidad de tener un divorcio con las razones mismas a los hombres - antes de aprobar este ley la mujer necesitaba proveer que el hombre había estado más que solo infidelidad - por ejemplo incesto o crueldad. 

# Hoy en día
## En España
- Acuerdo mutuo en España pero no Inglaterra
- El cambio más significante es que no es un proceso tan penoso si todas las partidas son de acuerdo. 
- La liberalisación de las relaciones personales y sexuales significa que el matrimonio no _tiene_ que durar la vida
- El aumento en el tiempo de vivir significa que se los matrimonios duran más tiempo
- Así que, es más facíl solicitar un divorcio y disolvear una relación personal 

## En Inglaterra
- Hemos tenido los cambios de misma manera
- El ley no ha llegado
- Se han estado graduales, no instantas, dado que no tenemos revoluciones 

# Conclusión 
- no hay otra conclusión que valga - el matrimonio cambia
- los valores no se han cambiado, pero más vale que el matrimonio insoportable se puede terminar que las personas no tienen que soportarlo. 


