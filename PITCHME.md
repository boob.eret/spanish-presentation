# El divorcio en España y el Reino Unido

---
## La historia de la ley del divorcio en España
@ul
- 2ª República Española - Reforme Social Total
- Franco
- Democracia - "Ley 30/1981, de 7 de julio"
- Divorcio Express 
@ulend



---

## El divorcio express 
@ul
- Cambio de la estructura social de España
- En 2006 (el primer año de Divorcio Express) subió el numero de divorcios por un 74,3%. 
@ulend

---

## La historia de la ley del divorcio en el Reino Unido

@ul

- Después de 1857 solo era para la gente rica
- "Matrimonial Causes Act 1857" 
- "Matrimonial Causes Act 1937" 

@ulend 

---

## Situación actual en España
@ul

- Unilateral
- Los ambos 

@ulend

---
## Situación actual en Reino Unido

@ul 
- Motivos:
    - Adultero
    - Comportamiento Irrazonable 
    - Deserción
    - Seperación de verdad por 2 años (acuerdo mutual)
    - Seperación de verdad por 5 años (acuerdo unilateral)
- No hay "ninguna culpa"
@ulend

---

# Conclusión
